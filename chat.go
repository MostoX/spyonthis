package main

import (
	"log"
	"net/http"
	"text/template"
	"code.google.com/p/go.net/websocket"
	"crypto/rand"
	"encoding/hex"
	"sync"
	"flag"
	"fmt"
	"time"
	"os"
	"strings"
	"io"
)

// Flag vars
var httpPort = flag.Int("httpPort", 443, "The port to use for http.")
var websPort = flag.Int("websPort", 5657, "The port to use for websockets.")
var debug = flag.Bool("debug", false, "Print debug info")
var temp *template.Template
var ticker = time.NewTicker(time.Hour * 1)
var socketsCounter = 0
var sMutex = &sync.RWMutex{}

type Client struct {
	socket *websocket.Conn
	expires time.Time
	done chan bool
}

// Clients map. Struct to hold a mutex
var clients = struct{
	sync.RWMutex
	m map[string]Client
}{m: make(map[string]Client)}


func main() {
	flag.Parse()
	if (*debug) {
		l("DEBUG")
	}
	dl(fmt.Sprintf("Starting server on http:%d and ws:%d\n", *httpPort, *websPort))


	// Set up the logging
	f, err := os.OpenFile("spyonthis.log", os.O_RDWR | os.O_CREATE | os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening log file: %v", err)
	}
	defer f.Close()
	//log.SetOutput(f)



	// Start the cleanup thread
	go cleanup()

	go startWebSocket()	// Start the websocket listener

	temp = template.Must(template.New("root").ParseFiles("chat.js"))

	// Start http handler just to handle serving the index and js
	dl("Starting web socket listener")
	http.HandleFunc("/chat.js", jsHandler)
	http.Handle("/", http.FileServer(http.Dir(".")))

	var httpAddr = fmt.Sprintf(":%d", *httpPort)
	httpErr := http.ListenAndServeTLS(httpAddr, "/root/keys/ivdatanet.crt", "/root/keys/ivdatanet.key", loggingHandler(http.FileServer(http.Dir("."))))
	if httpErr != nil { log.Fatal(httpErr) }
}

func startWebSocket() {
	dl("Starting http listener")
	http.Handle("/make", websocket.Handler(makerHandler))  // For making a new chat
	http.Handle("/join", websocket.Handler(joinerHandler)) // For joining a chat
	
	var websAddr = fmt.Sprintf(":%d", *websPort)
	wsErr := http.ListenAndServeTLS(websAddr, "/root/keys/ivdatanet.crt", "/root/keys/ivdatanet.key", nil)
	if wsErr != nil { log.Fatal(wsErr) }
}

func jsHandler(w http.ResponseWriter, r *http.Request) {
	temp.ExecuteTemplate(w, "chat.js", *websPort)
	logRequest(r)
}
func loggingHandler (h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if (strings.HasSuffix(r.URL.Path, ".js")) {
			jsHandler(w, r)
		} else {
			h.ServeHTTP(w, r)
		}
		logRequest(r)
	})
}

func makerHandler(ws *websocket.Conn) {
	dl("Got a user making a chat")
	sMutex.Lock()
	socketsCounter++
	sMutex.Unlock()
	// Create a hash
	hash := makeHash()
	l("New hash: " + hash)
	websocket.Message.Send(ws, "{\"hash\":\"" + hash + "\"}") // Sent to client to be shared

	// Create a client with their websocket handle
	c := Client{ws, time.Now().Add(time.Hour*1), make(chan bool)}

	// Add this client to the map of clients with the above hash as a key
	clients.Lock()
	clients.m[hash] = c
	str := fmt.Sprintf("Current entries in map: %d", len(clients.m))
	clients.Unlock()

	l(str)

	timeout := make(chan bool, 1)
	go func() {
    	time.Sleep(24 * time.Hour)
    	timeout <- true
	}()

	select {
		case <- timeout:
		case <- c.done:
	}
	sMutex.Lock()
	socketsCounter--
	sMutex.Unlock()
	dl("Maker websocket closed")
}

func joinerHandler(ws *websocket.Conn) {
	dl("Got a user joining")
	sMutex.Lock()
	socketsCounter++
	sMutex.Unlock()
	// Receive the hash they got from someone else
	var hash string
	websocket.Message.Receive(ws, &hash)
	l("Got hash from joiner: " + hash)

	// Retrieve the client that made this hash
	clients.RLock()
	c, exists := clients.m[hash]
	delete(clients.m, hash)
	clients.RUnlock()

	// Just copy signalling between sockets now
	if (!exists) {
		dl("This hash is not in the map. Returning...")
		return
	}
	dl("Starting copy...")
	errc := make(chan error, 1)
	go cp(ws,c.socket,errc)
	go cp(c.socket,ws,errc)
	
	timeout := make(chan bool, 1)
	go func() {
    	time.Sleep(24 * time.Hour)
    	timeout <- true
	}()

	select {
		case <- timeout:
		case <- errc:
			if err := <-errc; err != nil {
				log.Println(err)
			}
	}
	c.done <- true
	sMutex.Lock()
	socketsCounter--
	sMutex.Unlock()
	dl("Joiner websocket closed")
}

func cp(w *websocket.Conn, r *websocket.Conn, errc chan<- error) {
	_, err := io.Copy(w,r)
	errc <- err
}

func makeHash() (string) {
	rb := make([]byte,4)
	_, err := rand.Read(rb) 		// 10 byte random number
 	if err != nil { log.Fatal(err) }
	return hex.EncodeToString(rb) 	// return the 10 bytes encoded as a hex string
}

func l(message string) {
	log.Printf(message)
}

func dl(message string) {
	if *debug { log.Printf("DEBUG: " + message) }
}

func logRequest(r *http.Request) {
	l(r.RemoteAddr + " - " + r.Method + " " + r.RequestURI)
}

func cleanup() {
	for {
		select {
			case <- ticker.C:
				// Clean up hashes that have not been used in x mins
				l("Cleaning up...")
				now := time.Now()
				clients.Lock()
				for k, v := range clients.m {
					if (now.After(v.expires)) {
						delete(clients.m, k)
					}
				}
				str := fmt.Sprintf("Entries in map after cleanup: %d", len(clients.m))
				clients.Unlock()
				l(str)
				sMutex.Lock()
				str = fmt.Sprintf("Open websockets: %d", socketsCounter)
				sMutex.Unlock()
				l(str)
				// if time.Now().After(client.expires)
		}
	}
}
