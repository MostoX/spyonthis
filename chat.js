// If hostname == host we are the chat initiator
// If not, we are a joiner.


var servers = {
    iceServers: [
        {url: "stun:23.21.150.121"},
        {url: "stun:stun.l.google.com:19302"}
    ]
}; // Free google STUN servers

var channelOptions = {reliable: true}; // Set this to make a TCP data channel rather than UDP

// Browser hax
var RTCPeerConnection = window.mozRTCPeerConnection || window.webkitRTCPeerConnection || window.RTCPeerConnection;
var RTCSessionDescription = window.mozRTCSessionDescription || window.webkitRTCSessionDescription || window.RTCSessionDescription;
var RTCIceCandidate = window.mozRTCIceCandidate || window.webkitRTCIceCandidate || window.RTCIceCandidate;

var connectionState = 0;
var maker = (document.URL.split("?")[1] ? false : true);
var offer, peerConnection, chatBox, websocket, dataChannel, intVar, typingTimer = null;
var frontPageElements, chatPageElements, iconTag, typingTag; 
var inChat, isFlashing, isTyping = false;

var youPrefix = "<div class=\"you\">You:</div>  ";
var themPrefix = "<div class=\"them\">Them:</div> ";

var icontitle = [["favicon.png", "‾‾‾Spy On This‾‾‾"], ["altfavicon.png", "___Spy On This___"]];

var DEBUG = true;

window.addEventListener("beforeunload", function (e) {
	if (inChat) {
		(e || window.event).returnValue = "Are you sure you want to leave? You are still connected."; // Put your hands up for detroit
	 	return "Are you sure you want to leave? You are still connected."; 
	}
});


////////////////////////////////
// Misc functions (not optimised)
////////////////////////////////
function onload() {
	debugLog("The page has loaded.");

	frontPageElements = document.querySelectorAll(".frontpage");
	chatPageElements = document.querySelectorAll(".chatpage");
	typingTag = document.getElementById("typing");
	iconTag = document.getElementById("favicon");
	displayPage(maker);
	chatBox = document.getElementById("chat");
	document.getElementById("text").onkeyup = typing;

	debugLog("Creating a peer connection...");
	peerConnection = new RTCPeerConnection(servers);
	peerConnection.onicecandidate = gotIceCandidate;
	peerConnection.oniceconnectionstatechange = function () {
		if (peerConnection.iceConnectionState == "failed") {
			addText("Connecting to your peer failed. This may happen if you're on a company or university network that blocks connections.<br>Spy On This should work from your home.", chatBox);
			websocket.close();
		}
	}
	openSocket();

	if (maker) {
		debugLog("This client is for making a new chat.");
		debugLog("Creating a data channel..");
		gotDataChannel({"channel":peerConnection.createDataChannel("peerConnection", channelOptions)});
	} else {
		debugLog("This client is joining an existing chat.");
		peerConnection.ondatachannel = gotDataChannel;
		addText("Connecting...<br>", chatBox);
	}
}

function onSend() {
	if (document.getElementById("text").value != "") {
		dataChannel.send(JSON.stringify({"message":document.getElementById("text").value}));
		addText("<div>" + youPrefix + '<div class="message">' + linkURLs(encode(document.getElementById("text").value)) + "</div></div>", chatBox);
		document.getElementById("text").value = "";
		isTyping = false;
		clearTimeout(typingTimer);
	}
}

function openSocket() {
	debugLog("Opening websocket to server");
	var uri = "wss://" + window.location.hostname + ":{{.}}";
	
	if (maker) {
		console.log("We are creating a chat!");
		uri = uri + "/make";
	}
	else {
		maker = false;
		console.log("We are joining a chat!");
		uri = uri + "/join";
	}

	websocket = new WebSocket(uri);
	if (!maker) { // This is a joiner
		websocket.onopen = function(e) {
			websocket.send(document.URL.split("?")[1]); // Send the hash from the url as soon as the socket is open
		};
	}
	websocket.onmessage = function(e) {
		debugLog(e.data);
		message = JSON.parse(e.data);
		
		if (message.sdp) {
			debugLog("Got a session description from the other side..");
			peerConnection.setRemoteDescription(new RTCSessionDescription(message.sdp), function() {
				debugLog("remote desc set.");
				if (peerConnection.remoteDescription.type == "offer") {
					debugLog("What we got was an offer. We better reply..");
					peerConnection.createAnswer(function (desc) {
						debugLog("Created an answer");
						peerConnection.setLocalDescription(desc, function() {
							debugLog("Local desc set.");
							websocket.send(JSON.stringify({"sdp": peerConnection.localDescription}));
						}, function() {console.log("Error setting local desc");});
					}, function (error) {console.log(error);});
				}
			}, function() {console.log("Error setting remote desc");});
		} else if (message.candidate) { // We received an ICE candidate
			peerConnection.addIceCandidate(new RTCIceCandidate(message.candidate), function() {debugLog("added new ice candidate");}, function(error) {console.log(error);});
		} else if (message.hash) { // We received a server-generated hash for this session
			debugLog("Hash");
			document.getElementById("linkBox").value = "https://" + document.location.host + "/?" + message.hash;
			peerConnection.createOffer(gotOffer, function () {}); // Create and send an offer right away. No biggie if it's never used.
		}
	};

	websocket.onclose = function() { debugLog("Websocket closed"); };
	websocket.onerror = function(e) { console.log("Websocket error: " + e.data); }; // TODO: Print error to chat? Tell user to reload?
}
////////////////////////////////



////////////////////////////////
// WebRTC Callbacks
////////////////////////////////
function gotIceCandidate(event) {
	debugLog("Got ice candidate");
	if (event.candidate) {
		debugLog(event.candidate);
		websocket.send(JSON.stringify({"candidate": event.candidate}));
	}
}

function gotOffer(desc) { // We created an offer successfully. Set as local desc and send to the other side
	debugLog("got offer: " + desc.sdp);
	peerConnection.setLocalDescription(desc, function() {
		debugLog("Local desc set. Sending the desc.");
		websocket.send(JSON.stringify({"sdp": peerConnection.localDescription}));
	}, function() {console.log("Error setting local desc");});
}

function gotDataChannel(event) { // Got a webRTC channel
	console.log("Got a data channel");
	dataChannel = event.channel
	dataChannel.onmessage = function(event) { onDataChannelMessage(event.data); };
	dataChannel.onclose = function () {
		addText("<br>Your partner closed the chat.", chatBox);
		console.log("Data channel closed")
		document.getElementById("text").value = "";
		document.getElementById("text").disabled = true;
		document.getElementById("submit").disabled = true;
		inChat = false;
	};
	dataChannel.onopen = function () {
		if (maker) {
			displayPage(false);
			switchToChat();
		}
		addText("You are connected. Say hi!", chatBox);
		startFlash();
		inChat = true;
		websocket.close();
	};
}
////////////////////////////////



// VISUAL FUNCTIONS
function onDataChannelMessage(message) {
	message = JSON.parse(message);
	if (Object.keys(message)[0] == "message") {
		addText("<div>" + themPrefix + '<div class="message">' + linkURLs(encode(message.message)) + "</div></div>", chatBox);
		startFlash();
		typingTag.style.visibility = "hidden";
		typingTag.style.opacity = 0;
	} else if (Object.keys(message)[0] == "typing") {
		console.log("typing");
		typingTag.style.visibility = "visible";
		typingTag.style.opacity = 1;
	} else if (Object.keys(message)[0] == "stoppedtyping") {
		console.log("stopped typing");
		typingTag.style.visibility = "hidden";
		typingTag.style.opacity = 0;
	}
}

function displayPage(frontPage) {
	if (frontPage) {
		for (var i = 0; i < frontPageElements.length; i++) {
			frontPageElements[i].style.visibility = "visible";
			frontPageElements[i].style.opacity = 1;
		}
	} else {
		for (var i = 0; i < chatPageElements.length; i++) {
			chatPageElements[i].style.visibility = "visible";
			chatPageElements[i].style.opacity = 1;
		}
		switchToChat();
	}
};

function onStart() {
	debugLog("Start button pressed");
	document.getElementById("linkBox").style.visibility = "visible";
	document.getElementById("linkBox").select();
	document.getElementById("linkBox").style.opacity = 1;
	document.getElementById("startButton").style.visibility = "hidden";
	document.getElementById("startButton").style.opacity = 0;
	document.getElementById("buttonHint").style.visibility = "visible";
	document.getElementById("buttonHint2").style.visibility = "visible";
	document.getElementById("buttonHint").style.opacity = 1;
	document.getElementById("buttonHint2").style.opacity = 1;
	connectionState++; // Now wait for an answer
}

function switchToChat() {
	document.getElementById("title").style.paddingTop = "20px";

	document.getElementById("below").style.opacity = 0;
	document.body.style.backgroundColor = "#DDD"
	setTimeout(hideFrontPage, 1000);
}

function hideFrontPage() 
{	document.getElementById("linkBox").style.visibility = "hidden";
	document.getElementById("desc").style.visibility = "hidden";
	document.getElementById("below").style.visibility = "hidden";
}

function addText(text, box) {
	box.innerHTML = box.innerHTML + text;
	box.scrollTop = box.scrollHeight;
}

function onLinkCopy() {
	document.getElementById("buttonHint").style.opacity = 0;
	var elem = document.getElementById("buttonHint");
	var butt = elem.currentStyle || window.getComputedStyle(elem);
	document.getElementById("buttonHint2").style.marginBottom = butt.marginBottom;
}

function startFlash() {
	if (!isFlashing) {
		console.log("startflash");
		intVar = setInterval(function() {
			toggleFlash();
		}, 500);
		toggleFlash();
		document.addEventListener("mousemove", stopFlash);
		document.addEventListener("keydown", stopFlash);
		document.addEventListener("focus", stopFlash);
		isFlashing = true;
	}
}

function toggleFlash() {
	var a = icontitle.pop();
    document.title = a[1];
    iconTag.href = a[0];
    icontitle.unshift(a)
}

function stopFlash() {
	if (isFlashing) {
		console.log("stopflash");
		clearInterval(intVar);
		iconTag.href = "favicon.png";
		document.title = "Spy On This";
		document.removeEventListener("mousemove", stopFlash);
		document.removeEventListener("keydown", stopFlash);
		document.removeEventListener("focus", stopFlash);
		isFlashing = false;
	}
}

function typing(event) {
	if (document.getElementById("text").value == "" && isTyping) { // They've deleted everything in the box
		isTyping = false;
		dataChannel.send(JSON.stringify({"stoppedtyping": ""}));
	}
	else if (!isTyping && event.keyCode != 13) {
		isTyping = true;
		dataChannel.send(JSON.stringify({"typing": ""}));
		typingTimer = setTimeout(function() {
			isTyping = false;
			dataChannel.send(JSON.stringify({"stoppedtyping": ""}));
		}, 4000);
	} else {
		clearTimeout(typingTimer);
		typingTimer = setTimeout(function() {
			isTyping = false;
			dataChannel.send(JSON.stringify({"stoppedtyping": ""}));
		}, 4000);
	}
}

function encode(msg) {
	return msg.replace(/[\u00A0-\u9999<>\&]/gim, function(i) {
   		return '&#'+i.charCodeAt(0)+';';
})};

function debugLog(msg) {
	if (DEBUG) { console.log(msg); }
}

function linkURLs(text)
{
	var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
	return text.replace(exp,"<a href='$1' target='_blank'>$1</a>"); 
}